//
// Created by victor on 17-10-21.
//

#ifndef POUTINE_EXIT_H
#define POUTINE_EXIT_H


#include "item.h"

class Exit : public Item{

public:
    Exit(float x, float y);
    void Activate();
};


#endif //POUTINE_EXIT_H
