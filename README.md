#Jam Pixel 21-22 Octobre 2017
####Theme : Espionnage


####Principe du jeu

Les Nord coreens  mennent une guerre sans merci contre les Nazis depuis des generations. Pour mettre fin a cette guerre interminable les coreens decident d'espionner les nazis et de leur derober leur technologie. 

A toi de choisir, qui incarnera tu ?? Aidera tu les coreenes ou preferes tu voir les nazis l'emporte!

**Choisis ton camp**!


####Compilation
Deux libs sont necessaire pour compiler le jeu, la SDL2 avec les sous modules SDL_net, SDL2_image et libglew pour faire fonctionner OpenGL.

Il faut egalement necessairement dispose d'un compilo pouvant compiler le c++17, g++7 fait tres bien l'affaire.

####Jouer au jeu 
L'espion doit recuperer les documents et sortir par les bouches d'egouts.

Le gardien doit attraper l'espion.

Le deplacement se fait avec WASD.(et ouais on a un clavier qwerty)

####Lancer le jeu
Il faut lancer le serveur sur un ordi et le client sur l'autre. Celui qui lance le serveur est l'espion.

Lancer le serveur : ./Poutin localhost

Lancer le client : ./Poutin ipDuPcServeur

**IMPORTANT** Toujours lancer le serveur avant le client!


