//
// Created by stiven on 17-10-21.
//

#ifndef POUTINE_CONSTANT_H
#define POUTINE_CONSTANT_H
namespace constant{
    namespace window{
        constexpr int width = 768;
        constexpr int height = 768;
    }
    namespace uv{
        constexpr float deltaX = 0.1f;
        constexpr float deltaY = 0.1f;
    }
}
#endif //POUTINE_CONSTANT_H
