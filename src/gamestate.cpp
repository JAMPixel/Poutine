//
// Created by stiven on 17-10-21.
//

#include "inc/gamestate.h"
#include <map/item.h>
#include <SDL2/SDL_scancode.h>
#include <iostream>
#include <inc/network/messenger.h>

GameState::GameState() : spy(map), wMan(map) {

}

void GameState::loadLevel(std::string const &level) {

    map.createMap(level);
    sensorList.push_back({0,1});
}

void GameState::update(bool isSpy, glm::vec2 direction) {
    glm::vec2 newPos;
    bool isNew = Messenger::posMessage(newPos);
    if (isSpy) {
        spyLoop(direction);
        if(isNew){
            wMan.setPosition(newPos);
        }
    } else {
        guardLoop(direction);
        if(isNew){
            spy.setPosition(newPos);
        }
        isNew = Messenger::xFilesMessage(newPos);
        if(isNew){
            delete map.maps[newPos.x][newPos.y];
            map.maps[newPos.x][newPos.y] = nullptr;
        }
    }
}

const Map &GameState::getMap() const {
    return map;
}

const Spy &GameState::getSpy() const {
    return spy;
}

const Watchman &GameState::getWatchman() const {
    return wMan;
}

glm::vec2 GameState::manageInput(const Uint8 *key) {
    float delta = 0.2;
    glm::vec2 direction;
    if (key[SDL_SCANCODE_W]) {
        direction[1] = -delta;
    }
    if (key[SDL_SCANCODE_S]) {
        direction[1] = delta;
    }
    if (key[SDL_SCANCODE_A]) {
        direction[0] = -delta;
    }
    if (key[SDL_SCANCODE_D]) {
        direction[0] = delta;
    }

    return direction;


}


void GameState::manageMouse(int x, int y, bool isSpy) {

    //if(isSpy) {
        spy.changeRotation(glm::vec2{x, y});
    //}else {
        wMan.changeRotation(glm::vec2{x, y});
    //}
}

void GameState::spyLoop(glm::vec2 direction) {
    const Item *item = spy.move(direction);
    if (item && item->isXFile) {
        foundDocument = true;
        Messenger::send(Message<glm::vec2>{XFILES, glm::vec2{item->pos[1], item->pos[0]}});
        delete map.maps[item->pos[1]][item->pos[0]];
        map.maps[item->pos[1]][item->pos[0]] = nullptr;
        std::cout << "you found XFiles !" << std::endl;
    } else {
        if (item && item->isExit && foundDocument) {
            std::cout << "SPY WIN !" << std::endl;
            state = SPY_WIN;
        }
    }
    Messenger::send(Message<glm::vec2>{MOVE, spy.position});

}

void GameState::guardLoop(glm::vec2 direction) {
    const Item *item = wMan.move(direction);
    if (wMan.isSeeingSpy(spy.position)) {
        state = WATCHMAN_WIN;
        std::cout << "WATCHMAN WIN !" << std::endl;

    }
    Messenger::send(Message<glm::vec2>{MOVE, wMan.position});
    for(auto &s : sensorList){
        s.sens(spy.position);
    }

}
