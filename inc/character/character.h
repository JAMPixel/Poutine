//
// Created by victor on 17-10-21.
//

#ifndef POUTINE_CHARACTER_H
#define POUTINE_CHARACTER_H

#include <glm/glm.hpp>
#include "map/map.h"

class Character {
public:
    glm::vec2 position;
    glm::vec2 transformPosition;
    float speed = 0.20f;
    float size = 0.08f;
    Map const & map;
    glm::vec2 viewDirection{1, 0};
    float angle;
    Character(const Map &map);


public:
    void setPosition(glm::vec2 position);
    void changeRotation(glm::vec2 const & direction);
    virtual const Item * move(glm::vec2 direction);
};


#endif //POUTINE_CHARACTER_H
