//
// Created by victor on 17-10-21.
//

#ifndef POUTINE_WATCHMAN_H
#define POUTINE_WATCHMAN_H


#include "character.h"

class Watchman: public Character {
    float fieldOfViewMin = 1.f;
    float fieldOfViewMax = 1.5f;
    float distanceOfView = 2.f;

public:
    Watchman(const Map &map);
    bool countRec(glm::vec2 pos, int count, int orientation, glm::vec2 spyPos);

    const Item * move(glm::vec2 direction) override;
    bool isSeeingSpy(glm::vec2 spyPosition);


};


#endif //POUTINE_WATCHMAN_H
