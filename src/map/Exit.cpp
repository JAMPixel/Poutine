//
// Created by victor on 17-10-21.
//

#include "inc/map/Exit.h"

Exit::Exit(float x, float y) : Item(x, y, false, "door_closed") {
    isExit = true;
}

void Exit::Activate(){
    idTexture = "door_opened";
}