//
// Created by stiven on 17-10-21.
//

#include <inc/gamestate.h>
#include <inc/display/display.h>
#include <utils/quad.hpp>
#include <inc/utils/constant.h>
#include <inc/manager/textureManager.h>
#include <map/item.h>
Display::Display(const GameState &gameState) : gameState(gameState), screen(glish::normalizedScreenSpace((float)constant::window::width, (float)constant::window::height)) {
    TextureManager::init();
        program.init("scale", "transfo", "image", "floor", "object", "delta", "isPerso", "isFog", "pos");
        program.set<uni::scale>(screen);
        program.set<uni::floor>(TextureManager::getUv("floor0"));
        program.set<uni::delta>(glm::vec2(constant::uv::deltaX, constant::uv::deltaY));
        program.set<uni::image>(0);
        TextureManager::bind(0);
        vao.addVbo(0, glish::mat::rect);
        vao.addVbo(1, glish::mat::uv);
    vao.bind();

}

void Display::update(bool isSpy) {
    program.set<uni::delta>(glm::vec2(constant::uv::deltaX, constant::uv::deltaY));

    displayMap();
    displayCharacter();
    displayFogWar(isSpy);
}

void Display::displayMap() {

    Map const &map = gameState.getMap();
    for (int i = 0; i < map.getSizeY(); i++) {
        for (int j = 0; j < map.getSizeX(); j++) {
            transformStorage.tran.y = map.getDeltaY() * i;
            transformStorage.tran.x = map.getDeltaX() * j;
            transformStorage.scale = glm::vec2(map.getDeltaX(), map.getDeltaY());
            transformStorage.angle = 0;
            program.set<uni::transfo>(glish::transform(transformStorage));

            program.set<uni::isPerso>(false);

            Item * const item = map.getItem(i, j);
            if(item){
                program.set<uni::object>(TextureManager::getUv(item->getIdTexture()));
            }else{
                program.set<uni::object>(TextureManager::getUv("floor"+(std::to_string((i*2+1+j*3)%4))));

            }

//
            glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
//        }

        }

    }
}


// Point de codage : 8 booleens, chacun codant si un des voisins du mur est un mur. Ordre :
// 1 2 3
// 4 X 5
// 6 7 8
void Display::displayWall(const std::vector<bool> &){

    


}

void Display::displayCharacter() {

    // SPY
    transformStorage.tran = gameState.getSpy().transformPosition;
    transformStorage.scale = glm::vec2(gameState.getSpy().size*500);
    transformStorage.angle = gameState.getSpy().angle;
    program.set<uni::transfo>(glish::transform(transformStorage, 0.5, 0.5));
    program.set<uni::object>(TextureManager::getUv("perso"));
    program.set<uni::isPerso>(true);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    //WATCHMAN
    transformStorage.tran = gameState.getWatchman().transformPosition;
    transformStorage.scale = glm::vec2(gameState.getWatchman().size*500);
    transformStorage.angle = gameState.getWatchman().angle;
    program.set<uni::transfo>(glish::transform(transformStorage, 0.5, 0.5));
    program.set<uni::object>(TextureManager::getUv("watch"));
    program.set<uni::isPerso>(true);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

}

void Display::displayFogWar(bool isSpy) {

    transformStorage.tran = glm::vec2{};
    transformStorage.scale = glm::vec2{constant::window::width, constant::window::height};
    transformStorage.angle = 0;
    program.set<uni::isFog>(true);
    program.set<uni::transfo>(glish::transform(transformStorage));
    glm::vec2 scaling ;
    const Character * character = nullptr;
    if(isSpy){
        character = &gameState.getSpy();
    }else{
        character = &gameState.getWatchman();
    }
    scaling.x = character->transformPosition.x/constant::window::width;
    scaling.y = character->transformPosition.y/constant::window::height;
    program.set<uni::pos>(scaling);
    program.set<uni::object>(glm::vec2{0, 0});
    program.set<uni::delta>(glm::vec2(1, 1));

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    program.set<uni::isFog>(false);


}
