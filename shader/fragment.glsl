#version 400



out vec4 color;

in vec2 uvOut;
in vec2 uvDeux;
uniform sampler2D image;
uniform bool isPerso;
uniform bool isFog;
uniform vec2 pos;
void main(){
    if(!isFog){
        vec4 floor = texture(image, uvOut);
        vec4 onTop = texture(image, uvDeux);
        if(onTop.a == 0){
            color = floor;
        } else{
            color = onTop;
        }
        if(isPerso){
            color = onTop;
        }
    }else{
        if(length(pos-uvDeux) < 0.2){

            color.a =  length(pos-uvDeux)/0.2;
        }else{
            color = vec4(0, 0, 0, 0.97);
           }
    }

}