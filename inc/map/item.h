//
// Created by stiven on 17-10-21.
//

#ifndef POUTINE_ITEM_H
#define POUTINE_ITEM_H

#include <glm/vec2.hpp>
#include <string>
class Item{
protected:
    bool collidable;
    std::string idTexture;
public:
    glm::vec2 pos;
    const std::string &getIdTexture() const;

public:
    Item(float x, float y, bool collidable, std::string id= "floor0");
    Item(float x, float y);
    virtual const Item * action(bool isSpy) ;
    bool isCollidable() const ;
    bool isXFile = false;
    bool isExit = false;
};



#endif //POUTINE_ITEM_H
