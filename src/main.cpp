//
// Created by stiven on 17-10-21.
//

#include <inc/display/window.hpp>
#include <inc/display/windowSettings.hpp>
#include <GL/glew.h>
#include <inc/gamestate.h>
#include <inc/display/display.h>
#include <inc/utils/constant.h>
#include <inc/socket/socket.h>
#include <inc/socket/server.h>
#include <inc/network/messenger.h>

#define SOCKET
void gameLoopServer(Socket * socket){
#ifdef SOCKET
    Messenger::send(socket);
    Messenger::receive(socket);
#endif
}
void gameLoopClient(Socket * socket){
#ifdef SOCKET
    Messenger::receive(socket);
    Messenger::send(socket);

#endif

}
int main(int argc, char ** argv){


    glEngine::windowSettings settings = {"Poutine", 4, 0, constant::window::width, constant::window::height};
    glEngine::Window window{settings};
    if(SDLNet_Init()==-1) {
        printf("SDLNet_Init: %s\n", SDLNet_GetError());
        exit(2);
    }
    GameState gameState;
    gameState.loadLevel("assets/level/level0");
    Display display{gameState};
#ifdef SOCKET
    if(argc < 2){
        std::cerr << "error missing argument, needed 2" << std::endl;
        return 1;
    }
    bool server = false;
    bool isSpy = false;
    Socket * socket;
    if(!strcmp(argv[1], "localhost") || !strcmp(argv[1], "127.0.0.1")){
        socket = new Server;
        server = true;
        isSpy = true;
    }else{
        socket = new Socket(argv[1]);
    }

    socket->connect();
#endif

    bool run = true;
    SDL_Event ev {};
    // Boucle de jeu
    int time ;
    int end;
    int x, y;
    const Uint8 * key = SDL_GetKeyboardState(nullptr);
    while(run){
        time = SDL_GetTicks();
        glClear(GL_COLOR_BUFFER_BIT);
        // Recuperation Inputs
#ifdef SOCKET
        if(server){
            gameLoopServer(socket);
        }else{
            gameLoopClient(socket);
        }
#endif

        while(SDL_PollEvent(&ev)){
            switch (ev.type){
                case SDL_QUIT:
                    run = false;
                    break;
            }
        }

        if(key[SDL_SCANCODE_ESCAPE]){
            run = false;
        }
//        bool isSpy = false;
        SDL_GetMouseState(&x, &y);
        glm::vec2 direction = gameState.manageInput(key);
        gameState.manageMouse(x, y, isSpy);
        gameState.update(isSpy, direction);
        // Calcul et mise a jour de l'affichage

        display.update(isSpy);
        SDL_GL_SwapWindow(window);
        int end = SDL_GetTicks();
        if(end - time < 17){
            SDL_Delay(17-(end-time));
        }
    }

   // delete socket;
    SDLNet_Quit();
}