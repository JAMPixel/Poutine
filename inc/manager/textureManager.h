//
// Created by stiven on 17-10-21.
//

#ifndef TEXTURE_MANAGER
#define TEXTURE_MANAGER

#include <utility>
#include <glish/inc/glish/Texture.hpp>
#include <map>
#include <glm/vec2.hpp>


class TextureManager{
    static TextureManager textureManager;

    glish::Texture texture;
    std::map<std::string, glm::vec2> uv;
public:


    TextureManager();

/*template <class Path, class ...Next>
    static void fillMap(Path && path, Next && ... next){
        textureManager.textures.insert(std::make_pair(path, glish::Texture(std::forward<Path>(path))));
        if(sizeof...(next) != 0){
//            TextureManager::fillMap(std::forward<Next>(next)...);
        }
    }*/


    static void bind(int number);
    static glm::vec2 const & getUv(std::string const & key);
    static void init();

};

#endif