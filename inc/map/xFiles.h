//
// Created by victor on 17-10-21.
//

#ifndef POUTINE_XFILES_H
#define POUTINE_XFILES_H


#include "item.h"

class xFiles : public Item {
    const Item * action(bool isSpy) override;

public:
    xFiles(float x, float y);
};


#endif //POUTINE_XFILES_H
