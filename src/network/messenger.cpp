//
// Created by kelmyn on 17-10-21.
//

#include <inc/network/messenger.h>
#include <inc/socket/socket.h>
#include <network/message.h>
Messenger Messenger::instance;

Messenger::Messenger() {

}

std::unique_ptr<TypeMessage> Messenger::getLastMessage() {
    std::unique_ptr<TypeMessage> message;
    if(!instance.messageList.empty()){
        message  = std::move(instance.messageList.back());
        instance.messageList.erase(instance.messageList.end()-1);
    }
    return std::move(message);
}

void Messenger::send(Socket *socket) {
    std::unique_ptr<TypeMessage> message;

    while( (message = Messenger::getLastMessage())){
        socket->send(message.get());
        socket->receive();
        message = nullptr;
    }
    Message<int> fini {END, 2};
    socket->send(&fini);

}

void Messenger::receive(Socket *socket) {

    while(socket->receive()){
        Message<int> act {NONE, 2};
        socket->send(&act);
    }
}

void Messenger::posMessage(glm::vec2 const &pos) {
    instance.pos = new glm::vec2(pos);

}

bool Messenger::posMessage(glm::vec2 &pos) {
    if(instance.pos){
        pos = *instance.pos;
        delete instance.pos;
        instance.pos = nullptr;
        return true;
    }
    return false;
}

void Messenger::xFilesMessage(glm::vec2 const &pos) {
    instance.posXFiles = new glm::vec2(pos);

}

bool Messenger::xFilesMessage(glm::vec2 &pos) {
    if(instance.posXFiles){
        pos = *instance.posXFiles;
        delete instance.posXFiles;
        instance.posXFiles = nullptr;
        return true;
    }
    return false;
}
