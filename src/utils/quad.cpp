//
// Created by stiven aigle on 18/01/16.
//

#include <iostream>
#include <vector>
#include <glm/glm.hpp>
#include <utils/quad.hpp>

namespace glish {
    const std::vector<glm::vec3> mat::rect
            {
                    {0.0f, 0.0f, 1.0f},
                    {0.0f, 1.0f, 1.0f},
                    {1.0f, 0.0f, 1.0f},
                    {1.0f, 1.0f, 1.0f}
            };
    const std::vector<glm::vec2> mat::uv
            {
                    {0.0f, 0.0f},
                    {0.0f, 1.0f},
                    {1.0f, 0.0f},
                    {1.0f, 1.0f}
            };

    glm::mat3 transform(transformStorage const &store, float offsetX,
                        float offsetY) { //use evocative name to help you navigate in your code
        float cos0 = std::cos(glish::radian(store.angle));
        float sin0 = std::sin(glish::radian(store.angle));

        return glm::mat3 {store.scale.x * cos0, -store.scale.x * sin0, 0.0f,
                           store.scale.y * sin0, store.scale.y * cos0, 0.0f,
                           -offsetX * (store.scale.x * cos0 + store.scale.y * sin0) + store.tran.x,
                           -offsetY * (-store.scale.x * sin0 + store.scale.y * cos0) + store.tran.y, 1.0f
        };
    }
   /* glm::mat3 transform(transformStorage const &store, float cos0, float offsetX , float offsetY ){
        float sin0 = std::sqrt(1 - cos0*cos0);

        return glm::mat3 {store.scale.x * cos0, -store.scale.x * sin0, 0.0f,
                          store.scale.y * sin0, store.scale.y * cos0, 0.0f,
                          -offsetX * (store.scale.x * cos0 + store.scale.y * sin0) + store.tran.x,
                          -offsetY * (-store.scale.x * sin0 + store.scale.y * cos0) + store.tran.y, 1.0f
        };
    }*/

}