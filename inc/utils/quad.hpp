//
// Created by stiven aigle on 18/01/16.
//

#ifndef OPENGL_MATRICE_HPP
#define OPENGL_MATRICE_HPP

#include <glm/matrix.hpp>
#include <array>
#include <GL/glew.h>
#include <vector>
//TODO: changer le nom quad
namespace glish {
    /*! \struct transformStorage  "glish/quad.hpp"
     * \brief structure contenant les différentes transformation possible sur un quad
     *
     *\param tran : gère le déplacement horizontal:tran[0] et vertical:tran[1]\n
     *\param scale : gère le grossissement en x:scale[0] et en y:scale[1]\n
     *\param angle : gère la rotation, angle en degrès
     * */
    struct transformStorage {
        glm::vec2 tran{0.0f} ;
        glm::vec2 scale{1.0f};
        float angle = 0.0f;
    };

/** \file quad.hpp
 *contient toutes les fonctions necessaire aux transformations d'un rectangle\n
 * et à la normalisation du repère utilisé
 */
    namespace mat {
        /*! \var def
         * Coordonnées des vertices d'un rect situé sur le quart haut, droit de l'écran
         * elles sont données en coordonnées normalisées pour OpenGL
         * */
        extern const std::vector<glm::vec3> rect;
        /*Coordonnées uv, c'est à dire que ce sont les coordonnées que l'on donnee
         * généralement a un quad, ici le (0,0) se trouve en haut à gauche,
         * et le (1,1) est un bas à droite
         * */
        extern const std::vector<glm::vec2> uv;

    }



    glm::mat3 transform(transformStorage const &store, float offsetX = 0.0f, float offsetY = 0.0f);

//    glm::mat3 transform(transformStorage const &store, float cos0, float offsetX = 0.0f, float offsetY = 0.0f);

    /*Génere une matrice de normalisation de l'espace
     * c'est à dire que si on apliquer cette matrice aux vertices de notre quad,
     * on peut spécifier les coordonnées en pixel, avec comme origine le coin
     * supérieur gauche
     * */
    template<typename T>
    glm::mat3 normalizedScreenSpace(const T width, const T height, const T deplaceX = 1.0f, const T deplaceY = 1.0f) {
        glm::mat3 mat(
                2.0f / width, 0.0f, 0.0f,
                0.0f, -2.0f / height, 0.0f,
                -deplaceX, deplaceY, 1.0f
        );
        return mat;
    }

    template <typename T>
    float radian(T angle){
        return angle*M_PI/180;
    }



}





#endif //OPENGL_MATRICE_HPP
