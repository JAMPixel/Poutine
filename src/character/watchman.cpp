//
// Created by victor on 17-10-21.
//

#include <iostream>
#include "inc/character/watchman.h"
#include "map/item.h"

Watchman::Watchman(const Map &map) : Character(map) {
    position.x = 6;
    position.y = 6;

    transformPosition[0] = 300;
    transformPosition[1] = 300;

    viewDirection.x = 300;
    viewDirection.y = 0;

    std::cout<< "    "<< transformPosition.x << std::endl;
}

const Item *Watchman::move(glm::vec2 direction) {
    Character::move(direction);

    glm::vec2 tile(std::round(position[0]), std::round(position[1]));

    if (map.getItem(tile[1], tile[0])) {
        return map.getItem(tile[1], tile[0])->action(false);
    }
    return nullptr;


}


bool Watchman::countRec(glm::vec2 pos, int count, int orientation, glm::vec2 spyPos) {


    if (pos.x < 0 || pos.y < 0 || pos.x > map.getSizeY() -1 || pos.y > map.getSizeX() -1 ||
        (map.maps[pos.y][pos.x] && map.maps[pos.y][pos.x]->isCollidable())) {
        return false;
    }

    if (std::round(spyPos.x) == pos.x && std::round(spyPos.y) == pos.y) {
        return true;
    }

    switch (count) {
        case 0:
            switch (orientation) {
                case 0:
                    return countRec(glm::vec2(pos.x + 1, pos.y), 1, orientation, spyPos) ||
                           countRec(glm::vec2(pos.x - 1, pos.y), 2, orientation, spyPos) ||
                           countRec(glm::vec2(pos.x, pos.y - 1), 3, orientation, spyPos);
                    break;
                case 1:
                    return countRec(glm::vec2(pos.x, pos.y + 1), 1, orientation, spyPos) ||
                           countRec(glm::vec2(pos.x, pos.y - 1), 2, orientation, spyPos) ||
                           countRec(glm::vec2(pos.x + 1, pos.y), 3, orientation, spyPos);
                    break;
                case 2:
                    return countRec(glm::vec2(pos.x - 1, pos.y), 1, orientation, spyPos) ||
                           countRec(glm::vec2(pos.x + 1, pos.y), 2, orientation, spyPos) ||
                           countRec(glm::vec2(pos.x, pos.y + 1), 3, orientation, spyPos);
                    break;
                case 3:
                    return countRec(glm::vec2(pos.x, pos.y - 1), 1, orientation, spyPos) ||
                           countRec(glm::vec2(pos.x, pos.y + 1), 2, orientation, spyPos) ||
                           countRec(glm::vec2(pos.x - 1, pos.y), 3, orientation, spyPos);
                    break;
            }
            break;
        case 1:
            switch (orientation) {
                case 0:
                    return countRec(glm::vec2(pos.x, pos.y - 1), 4, orientation, spyPos);
                    break;
                case 1:
                    return countRec(glm::vec2(pos.x + 1, pos.y), 4, orientation, spyPos);
                    break;
                case 2:
                    return countRec(glm::vec2(pos.x, pos.y + 1), 4, orientation, spyPos);
                    break;
                case 3:
                    return countRec(glm::vec2(pos.x - 1, pos.y), 4, orientation, spyPos);
                    break;
            }
            break;
        case 2:
            switch (orientation) {
                case 0:
                    return countRec(glm::vec2(pos.x, pos.y - 1), 5, orientation, spyPos);
                    break;
                case 1:
                    return countRec(glm::vec2(pos.x + 1, pos.y), 5, orientation, spyPos);
                    break;
                case 2:
                    return countRec(glm::vec2(pos.x, pos.y + 1), 5, orientation, spyPos);
                    break;
                case 3:
                    return countRec(glm::vec2(pos.x - 1, pos.y), 5, orientation, spyPos);
                    break;
            }
            break;
        case 3:
            switch (orientation) {
                case 0:
                    return countRec(glm::vec2(pos.x + 1, pos.y - 1), 6, orientation, spyPos) ||
                           countRec(glm::vec2(pos.x - 1, pos.y - 1), 6, orientation, spyPos) ||
                           countRec(glm::vec2(pos.x, pos.y - 1), 6, orientation, spyPos);
                    break;

                case 1:
                    return countRec(glm::vec2(pos.x + 1, pos.y - 1), 6, orientation, spyPos) ||
                           countRec(glm::vec2(pos.x + 1, pos.y + 1), 6, orientation, spyPos) ||
                           countRec(glm::vec2(pos.x + 1, pos.y), 6, orientation, spyPos);
                    break;

                case 2:
                    return countRec(glm::vec2(pos.x - 1, pos.y + 1), 6, orientation, spyPos) ||
                           countRec(glm::vec2(pos.x + 1, pos.y + 1), 6, orientation, spyPos) ||
                           countRec(glm::vec2(pos.x, pos.y + 1), 6, orientation, spyPos);
                    break;

                case 3:
                    return countRec(glm::vec2(pos.x - 1, pos.y + 1), 6, orientation, spyPos) ||
                           countRec(glm::vec2(pos.x - 1, pos.y - 1), 6, orientation, spyPos) ||
                           countRec(glm::vec2(pos.x - 1, pos.y), 6, orientation, spyPos);
                    break;
            }
            break;
        case 4:
            switch (orientation) {
                case 0:
                    return countRec(glm::vec2(pos.x + 1, pos.y - 1), 6, orientation, spyPos) ||
                           countRec(glm::vec2(pos.x, pos.y - 1), 6, orientation, spyPos);
                    break;

                case 1:
                    return countRec(glm::vec2(pos.x + 1, pos.y + 1), 6, orientation, spyPos) ||
                           countRec(glm::vec2(pos.x + 1, pos.y), 6, orientation, spyPos);
                    break;

                case 2:
                    return countRec(glm::vec2(pos.x - 1, pos.y + 1), 6, orientation, spyPos) ||
                           countRec(glm::vec2(pos.x, pos.y + 1), 6, orientation, spyPos);
                    break;

                case 3:
                    return countRec(glm::vec2(pos.x - 1, pos.y - 1), 6, orientation, spyPos) ||
                           countRec(glm::vec2(pos.x - 1, pos.y), 6, orientation, spyPos);
                    break;
            }
            break;
        case 5:
            switch (orientation) {
                case 0:
                    return countRec(glm::vec2(pos.x - 1, pos.y - 1), 6, orientation, spyPos) ||
                           countRec(glm::vec2(pos.x, pos.y - 1), 6, orientation, spyPos);
                    break;

                case 1:
                    return countRec(glm::vec2(pos.x + 1, pos.y - 1), 6, orientation, spyPos) ||
                           countRec(glm::vec2(pos.x + 1, pos.y), 6, orientation, spyPos);
                    break;

                case 2:
                    return countRec(glm::vec2(pos.x + 1, pos.y + 1), 6, orientation, spyPos) ||
                           countRec(glm::vec2(pos.x, pos.y + 1), 6, orientation, spyPos);
                    break;

                case 3:
                    return countRec(glm::vec2(pos.x - 1, pos.y + 1), 6, orientation, spyPos) ||
                           countRec(glm::vec2(pos.x - 1, pos.y), 6, orientation, spyPos);
                    break;
            }
            break;
        case 6:
            switch (orientation) {
                case 0:
                    return countRec(glm::vec2(pos.x, pos.y - 1), 6, orientation, spyPos);
                    break;

                case 1:
                    return countRec(glm::vec2(pos.x + 1, pos.y), 6, orientation, spyPos);
                    break;

                case 2:
                    return countRec(glm::vec2(pos.x, pos.y + 1), 6, orientation, spyPos);
                    break;

                case 3:
                    return countRec(glm::vec2(pos.x - 1, pos.y), 6, orientation, spyPos);
                    break;
            }
            break;
        default:
            return false;
    }
}


bool Watchman::isSeeingSpy(glm::vec2 spyPosition) {


    if (std::abs(viewDirection.x) > std::abs(viewDirection.y))
        if (viewDirection.x > 0)
            return countRec(glm::vec2(std::round(position.x + 1), std::round(position.y)), 0, 1, spyPosition);
        else return countRec(glm::vec2(std::round(position.x - 1), std::round(position.y)), 0, 3, spyPosition);
    else if (viewDirection.y > 0)
        return countRec(glm::vec2(std::round(position.x), std::round(position.y + 1)), 0, 2, spyPosition);
    else return countRec(glm::vec2(std::round(position.x), std::round(position.y - 1)), 0, 0, spyPosition);
//
//    glm::vec2 projt = glm::dot(view, spy) * view;
//    glm::vec2 hauteur = spy - projt;
//
//    //std::cout << glm::length(projt)<< "   pos "<< viewDirection[0]<< "  "<< viewDirection[1]  <<  "  "<< glm::dot(projt,spy) << std::endl;
//    if (glm::length(projt) > distanceOfView || glm::dot(projt,spy)){
//        return false;
//
//    } else {
//        float dist = glm::length(hauteur);
//        float len = glm::length(projt);
//        if (dist > (1- len)* fieldOfViewMin + len * fieldOfViewMax) {
//            return false;
//        } else {
//            return true;
//        }
//    }


}
