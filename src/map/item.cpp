//
// Created by stiven on 17-10-21.
//
#include "map/item.h"

bool Item::isCollidable() const {
    return collidable;
}


Item::Item(float x, float y, bool collidable, std::string id) : pos(x, y), collidable(collidable), idTexture(id){

}

Item::Item(float x, float y) : pos(x, y), collidable(false){

}

const Item * Item::action(bool isSpy) {
    return this;
}

const std::string &Item::getIdTexture() const {
    return idTexture;
}

