//
// Created by stiven on 18/03/16.
//

namespace glish {

    template <typename T>
    void Uniform<T>::operator=(const int value){
        static_assert(std::is_same<type, int>::value, "Uniform's type is not int");
        prog->useProgram();
        glUniform1i(uni, value);
        getError();
    }
    template <typename T>
    void Uniform<T>::operator=(const float value){
        static_assert(std::is_same<type, float>::value, "Uniform's type is not float");
        prog->useProgram();
        glUniform1f(uni, value);
        getError();
    }
    template <typename T>
    void Uniform<T>::operator=(const double value){
        static_assert(std::is_same<type, double>::value, "Uniform's type is not double");
        prog->useProgram();
        glUniform1d(uni, value);
        getError();
    }
    template <typename T>
    void Uniform<T>::operator=(const glm::vec2 &value) {
        static_assert(std::is_same<type, glm::vec2>::value, "Uniform's type is not glm::vec2");

        prog->useProgram();
        glUniform2fv(uni, 1, glm::value_ptr(value));
        getError();
    }

    template <typename T>
    void Uniform<T>::operator=(const glm::vec3 &value) {
        static_assert(std::is_same<type, glm::vec3>::value, "Uniform's type is not glm::vec3");
        prog->useProgram();
        glUniform3fv(uni, 1, glm::value_ptr(value));
        getError();
    }

    template <typename T>
    void Uniform<T>::operator=(const glm::mat3 &value) {
        static_assert(std::is_same<type, glm::mat3>::value, "Uniform's type is not glm::mat3");
        prog->useProgram();
        glUniformMatrix3fv(uni, 1, GL_FALSE, glm::value_ptr(value));
        getError();
    }

    template <typename T>
    void Uniform<T>::operator=(const glm::mat4 &value) {
        static_assert(std::is_same<type, glm::mat4>::value, "Uniform's type is not glm::mat4");
        prog->useProgram();
        glUniformMatrix4fv(uni, 1, GL_FALSE, glm::value_ptr(value));
        getError();

    }

    template <typename T>
    void Uniform<T>::operator=(const bool value) {
        static_assert(std::is_same<type, bool>::value, "Uniform's type is not bool");
        prog->useProgram();
        glUniform1i(uni, value);
        getError();
    }

    template <typename T>
    void Uniform<T>::operator=(const glm::mat2 &value) {
        static_assert(std::is_same<type, glm::mat2>::value, "Uniform's type is not glm::mat2");
        prog->useProgram();
        glUniformMatrix2fv(uni, 1, GL_FALSE, glm::value_ptr(value));
        getError();

    }

    template <typename T>
    void Uniform<T>::operator=(const glm::vec4 &value) {
        static_assert(std::is_same<type, glm::vec4>::value, "Uniform's type is not glm::vec4");
        prog->useProgram();
        glUniform4fv(uni, 1, glm::value_ptr(value));


    }


}

