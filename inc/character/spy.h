//
// Created by victor on 17-10-21.
//

#ifndef POUTINE_SPY_H
#define POUTINE_SPY_H


#include "character.h"

class Spy: public Character {
public:
    Spy(const Map &map);

    const Item * move(glm::vec2 direction) override ;



};


#endif //POUTINE_SPY_H
