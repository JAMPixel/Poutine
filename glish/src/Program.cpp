//
// Created by stiven on 14/12/15.
//


#include <fstream>
#include <iostream>
#include <vector>
#include <assert.h>
#include <GL/glew.h>
#include <glish/utils//log.hpp>
#include <glish/Program.hpp>

namespace glish {
    Program::Program() {
    }
    void Program::useProgram() {
        glUseProgram(programID);
        getError();

    }

    GLuint Program::getProgram() {
        return programID;
    }

    Program::~Program() {
        if(programID) {
            glDeleteProgram(programID);
            getError();
        }
    }

    std::string Program::extractShader(const char *path) {

        std::string shaderCode;
        std::ifstream shaderStream(path);
        std::string shader(path);
        if(!shaderStream.is_open()){
            throw std::runtime_error("probleme ouverture fichier "+ shader);
        }
        std::string read;
        while(getline(shaderStream, read)){
            shaderCode += "\n" + read;
        }
        std::cout << "compiling shader " << shader << std::endl;
        return shaderCode;
    }
    GLuint Program::compileShader(const std::string &shader, GLenum type) {
        GLuint shaderId = glCreateShader(type);
        getError();
        char const * shaderSource = shader.c_str();
        GLint result = GL_FALSE;
        int infoLog;
        glShaderSource(shaderId, 1, &shaderSource, NULL);
        getError();
        glCompileShader(shaderId);
        glGetShaderiv(shaderId, GL_COMPILE_STATUS, &result);
        getError();
        glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &infoLog);
        getError();
        if (infoLog > 0) {
            std::vector<char> VertexShaderErrorMessage(infoLog + 1);
            glGetShaderInfoLog(shaderId, infoLog, NULL, &VertexShaderErrorMessage[0]);
            getError();
            std::cout << &VertexShaderErrorMessage[0] << std::endl;
        }
        return shaderId;
    }

    Program::Program(char const *vertexShader, char const *fragmentShader) {
        init(vertexShader, fragmentShader);


    }

    Program::Program(char const *vertexShader, char const *fragmentShader, char const *geometryShader) {
        init(vertexShader, fragmentShader, geometryShader);

    }

    void Program::compileProgram(GLuint &shader) {
        {
            glAttachShader(programID, shader);
            getError();
            glLinkProgram(programID);
            getError();
            glDetachShader(programID, shader);
            getError();
            glDeleteShader(shader);
            getError();
            GLint Result;
            int InfoLogLength;
            glGetProgramiv(programID, GL_LINK_STATUS, &Result);
            getError();
            glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &InfoLogLength);
            getError();
            if (InfoLogLength > 0) {
                std::vector<char> ProgramErrorMessage(InfoLogLength + 1);
                glGetProgramInfoLog(programID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
                printf("%s\n", &ProgramErrorMessage[0]);
                getError();
            }

        }
    }

    void Program::init(char const *vertexShader, char const *fragmentShader) {

        // Création des Shaders
        GLuint vertexShaderID = compileShader(vertexShader, GL_VERTEX_SHADER);
        getError();
        GLuint framentShaderID = compileShader(fragmentShader, GL_FRAGMENT_SHADER);

        // Link the program
        std::cout << "Linking program\n";
        programID = glCreateProgram();
        getError();
        compileProgram(vertexShaderID, framentShaderID);
    }

    void Program::init(char const *vertexShader, char const *fragmentShader, char const *geometryShader) {
        // Création des Shaders
        GLuint vertexShaderID = compileShader(vertexShader, GL_VERTEX_SHADER);
        getError();
        GLuint framentShaderID = compileShader(fragmentShader, GL_FRAGMENT_SHADER);
        GLuint geometryShaderId = compileShader(geometryShader, GL_GEOMETRY_SHADER);

        // Link the program
        std::cout << "Linking program\n";
        programID = glCreateProgram();
        getError();
        compileProgram(vertexShaderID, framentShaderID, geometryShaderId);
    }

    Program &Program::operator=(Program &&prog) {
        programID = prog.programID;
        prog.programID = 0;
        return *this;
    }

    Program::Program(Program &&prog) :programID(prog.programID){
        prog.programID = 0;

    }
}