//
// Created by kelmyn on 17-10-21.
//

#ifndef POUTINE_MESSAGE_H
#define POUTINE_MESSAGE_H

#include <vector>

enum MESSAGE_TYPE
{
    // Acquittement
    NONE,
    // Deplacement
    MOVE,
    // Action specifique au personnage
    ACT ,
    // Si bouton d'action general utilise
    INTERACT_WITH_ENVIRONEMENT,

    END,
    XFILES
};

class TypeMessage{
public:
    MESSAGE_TYPE const type;

    TypeMessage(MESSAGE_TYPE type);
    virtual void test(){}

};

template <typename T>
class Message : public TypeMessage
{
public:
    Message(MESSAGE_TYPE type, const T & data): TypeMessage(type), messageData(data){}

    const T messageData;
};

#endif //POUTINE_MESSAGE_H