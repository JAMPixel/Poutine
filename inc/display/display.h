//
// Created by stiven on 17-10-21.
//

#ifndef POUTINE_DISPLAY_H
#define POUTINE_DISPLAY_H

#include <inc/gamestate.h>
#include <glish/inc/glish/utils/ProgramGL.hpp>

#include <utils/quad.hpp>
#include <glish/inc/glish/Vao.hpp>

enum class uni{
     scale, transfo, image, floor, object, delta, isPerso, isFog, pos
};
class Display{

    const GameState & gameState;
    glish::ProgramGL<uni, glm::mat3, glm::mat3, int, glm::vec2, glm::vec2, glm::vec2, bool, bool,
            glm::vec2> program{glish::shaderFile{GL_VERTEX_SHADER, "shader/vertex.glsl"}, glish::shaderFile{GL_FRAGMENT_SHADER, "shader/fragment.glsl"}};
    glm::mat3 screen;
    glish::Vao<2> vao;
    glish::transformStorage transformStorage;
    void displayMap();
    void displayCharacter();
    void displayWall(const std::vector<bool> &);
    void displayFogWar(bool isSpy);

public:
    Display(const GameState &gameState);

    void update(bool isSpy);



};
#endif //POUTINE_DISPLAY_H
