//
// Created by stiven on 17-10-21.
//
#include <manager/textureManager.h>

TextureManager TextureManager::textureManager;

void TextureManager::bind(int number) {
    textureManager.texture.bindTexture(number);
}

TextureManager::TextureManager(){
    uv.insert(std::make_pair("perso", glm::vec2(0.0f, 0.1f)));
    uv.insert(std::make_pair("computer", glm::vec2(0.2f, 0.4f)));
    uv.insert(std::make_pair("door_closed", glm::vec2(0.0f, 0.5f)));
    uv.insert(std::make_pair("door_opened", glm::vec2(0.1f, 0.5f)));
    uv.insert(std::make_pair("floor0", glm::vec2(0.0f, 0.2f)));
    uv.insert(std::make_pair("floor1", glm::vec2(0.1f, 0.2f)));
    uv.insert(std::make_pair("floor2", glm::vec2(0.2f, 0.2f)));
    uv.insert(std::make_pair("floor3", glm::vec2(0.3f, 0.2f)));
    uv.insert(std::make_pair("dead", glm::vec2(0.0f, 0.3f)));
    uv.insert(std::make_pair("watch", glm::vec2(0.0f, 0.0f)));
    uv.insert(std::make_pair("sensor", glm::vec2(0.3f, 0.4f)));
}

glm::vec2 const &TextureManager::getUv(std::string const &key) {
    return textureManager.uv[key];
}

void TextureManager::init() {
    textureManager.texture = glish::Texture("assets/sprites/basic_wip.png");
}
