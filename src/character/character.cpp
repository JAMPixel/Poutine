//
// Created by victor on 17-10-21.
//

#include <inc/map/item.h>
#include <iostream>
#include <inc/network/messenger.h>
#include "inc/character/character.h"

const Item * Character::move(glm::vec2 direction) {


    glm::vec2 wantedPosition = position + direction * speed;
    glm::vec2 tile (std::round(position[0]),std::round(position[1]));
    bool cutRight = false;
    bool cutLeft = false;
    bool cutUp = false;
    bool cutDown = false;

    //on depasse a droite
    if (wantedPosition[0] + size - tile[0] > 0.5f) {
        cutRight = true;
    }if (wantedPosition[0] - size - tile[0] < -0.5f) {
        cutLeft = true;
    }if (wantedPosition[1] + size - tile[1] > 0.5f) {
        cutUp = true;
    }if (wantedPosition[1] - size - tile[1] < -0.5f) {
        cutDown = true;
    }



    // coupe a droite ?
    if(cutRight && (tile[0] + 1 > map.getSizeX() - 1 || (map.getItem(tile[1], tile[0] + 1 ) && map.getItem(tile[1], tile[0] + 1)->isCollidable()))){
        wantedPosition[0] = tile[0] + 0.49f - size;
    }
    //coupe a gauche ?
    if(cutLeft && (tile[0] - 1 < 0 || (map.getItem(tile[1], tile[0] - 1) && map.getItem(tile[1], tile[0] - 1)->isCollidable()))){
        wantedPosition[0] = tile[0] - 0.49f + size;
    }
    // coupe en haut ?
    if(cutUp && (tile[1] + 1 > map.getSizeY() - 1 || (map.getItem(tile[1] + 1, tile[0] ) && map.getItem(tile[1] + 1, tile[0] )->isCollidable()))){
        wantedPosition[1] = tile[1] + 0.49f - size;
    }
    // coupe en bas ?
    if(cutDown && (tile[1] - 1 < 0 || (map.getItem(tile[1] - 1, tile[0]) && map.getItem(tile[1] - 1, tile[0])->isCollidable()))){
        wantedPosition[1] = tile[1] - 0.49f + size;
    }

//
//    //coupe en haut a droite ?
//    if(cutRight && cutUp && !(tile[0] + 1 > map.getSizeX() - 1 || tile[1] + 1 > map.getSizeY() - 1  )&& map.getItem(tile[1] + 1, tile[0] + 1) && map.getItem(tile[1] + 1, tile[0] + 1)->isCollidable()){
//        wantedPosition[0] = tile[0] + 0.49f - size;
//        wantedPosition[1] = tile[1] + 0.49f - size;
//    }
//
//    if(cutRight && cutDown && !(tile[0] + 1 > map.getSizeX() - 1 || tile[1] - 1 < 0  ) && map.getItem(tile[1] - 1, tile[0] + 1) && map.getItem(tile[1] - 1, tile[0] + 1)->isCollidable()){
//        wantedPosition[0] = tile[0] + 0.49f - size;
//        wantedPosition[1] = tile[1] - 0.49f + size;
//    }
//
//    if(cutLeft && cutUp && !(tile[0] - 1 < 0  || tile[1] + 1 > map.getSizeY() - 1  )&& map.getItem(tile[1] + 1, tile[0] - 1) && map.getItem(tile[1] + 1, tile[0] - 1)->isCollidable()){
//        wantedPosition[0] = tile[0] - 0.49f + size;
//        wantedPosition[1] = tile[1] + 0.49f - size;
//    }
//
//    if(cutLeft && cutDown && !(tile[0] - 1 < 0  || tile[1] - 1 < 0  )&& map.getItem(tile[1] - 1, tile[0] - 1) && map.getItem(tile[1] - 1, tile[0] - 1)->isCollidable()){
//        wantedPosition[0] = tile[0] - 0.49f + size;
//        wantedPosition[1] = tile[1] - 0.49f + size;
//    }

    setPosition(wantedPosition);

//    Messenger::send(Message<glm::vec2>{MOVE, position});

}

Character::Character(const Map &map) : map(map) {}

void Character::changeRotation(glm::vec2 const &direction) {


    float delta_y = transformPosition.y - direction.y;
    float delta_x = transformPosition.x - direction.x;
    angle = atan2(delta_x, delta_y)*180/3.14;

    viewDirection = transformPosition - direction;


}

void Character::setPosition(glm::vec2 position) {
    this->position = position;
    transformPosition[0] = map.getDeltaX() * (position[0] +0.42f);
    transformPosition[1] = map.getDeltaY() * (position[1] +0.42f);
}
