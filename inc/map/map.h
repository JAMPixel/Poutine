//
// Created by stiven on 17-10-21.
//

#ifndef POUTINE_MAP_H
#define POUTINE_MAP_H

#include <vector>
#include <string>
#include <memory>
class Item;
class Map{

public :std::vector<std::vector<Item*>> maps;
private :using const_iterator = std::vector<std::vector<Item*>>::const_iterator;
    int deltaX;
    int deltaY;
public:
    int getDeltaX() const;

    int getDeltaY() const;

public:
    Map();
    void createMap(std::string const & path);
    void generateItem();
    const_iterator begin() const;
    const_iterator end() const;
    int getSizeX() const;
    int getSizeY() const;
    virtual ~Map();

    Item * const  getItem(int x, int y) const;

};
#endif //POUTINE_MAP_H
