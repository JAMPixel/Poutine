//
// Created by victor on 17-10-21.
//

#ifndef POUTINE_SENSOR_H
#define POUTINE_SENSOR_H


#include "item.h"

class sensor : public Item{
public:
    sensor(float x, float y);
    int sens(glm::vec2);
    glm::vec2 shakePos = pos;
};


#endif //POUTINE_SENSOR_H
