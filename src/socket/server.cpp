#include <inc/socket/server.h>

//
// Created by stiven on 17-10-21.
Server::Server():Socket() {

    if(SDLNet_ResolveHost(&ip, nullptr, 5000)==-1) {
        printf("SDLNet_ResolveHost: %s\n", SDLNet_GetError());
        exit(1);
    }
    tcpsock=SDLNet_TCP_Open(&ip);
    if(!tcpsock) {
        printf("SDLNet_TCP_Open Server: %s\n", SDLNet_GetError());
        exit(2);
    }

}

void Server::connect() {

    Socket::tcpsock = SDLNet_TCP_Accept(tcpsock);
    while(!Socket::tcpsock) {
        printf("SDLNet_TCP_Accept: %s\n", SDLNet_GetError());
        Socket::tcpsock=SDLNet_TCP_Accept(tcpsock);

    }

}

Server::~Server() {
    SDLNet_TCP_Close(tcpsock);
}
