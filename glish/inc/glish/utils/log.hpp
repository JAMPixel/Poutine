
//
// Created by darkblinx on 29/06/17.
//

#ifndef GLENGINE_LOG_HPP
#define GLENGINE_LOG_HPP

#include <iostream>
#include <cassert>
#include <GL/glu.h>



inline void getError() {
    GLenum error = glGetError();
    if (error) {
        std::cout << gluErrorString(error) << std::endl;
        assert(false);
    }
}
#endif //GLENGINE_LOG_HPP

