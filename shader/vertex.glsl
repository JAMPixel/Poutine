#version 400

layout(location = 0) in vec2 pos;
layout(location = 1) in vec2 uv;


uniform mat3 scale;
uniform mat3 transfo;
uniform vec2 floor;
uniform vec2 object;
uniform vec2 delta;
out vec2 uvOut;

out vec2 uvDeux;
void main(){

       gl_Position = vec4(scale*transfo*vec3(pos, 1), 1);
       uvDeux = mix(object, object+delta, uv);
       uvOut = mix(floor, floor+delta, uv);
}