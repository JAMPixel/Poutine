//
// Created by stiven on 17-10-21.
//

#ifndef POUTINE_SOCKET_H
#define POUTINE_SOCKET_H

#include <SDL2/SDL_net.h>
#include <inc/network/message.h>
#include <bits/unique_ptr.h>

class Socket{
protected:
    IPaddress ip;
    TCPsocket tcpsock;
    static constexpr unsigned sizeMax = 200;
public:

    Socket(const char *ipAdress);

    Socket();
    void send(TypeMessage *message) ;
    bool receive();
    virtual void connect();
    virtual ~Socket();

};
#endif //POUTINE_SOCKET_H
