//
// Created by darkblinx on 29/06/17.
//

#ifndef GLENGINE_WINDOWSETTINGS_HPP
#define GLENGINE_WINDOWSETTINGS_HPP

#include <string>
namespace glEngine {

    struct windowSettings{

        std::string title{"no title"};
        unsigned majorVersion = 3;
        unsigned minorVersion = 3;
        int width = 800;
        int height = 600;
        bool doubleBuffer = true;
        unsigned depthBits = 8;
        unsigned stencilBits = 1;


    };
}
#endif //GLENGINE_WINDOWSETTINGS_HPP
