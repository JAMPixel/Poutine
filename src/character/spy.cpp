//
// Created by victor on 17-10-21.
//

#include "inc/character/spy.h"
#include "map/item.h"

Spy::Spy(const Map &map) : Character(map) {}

const Item * Spy::move(glm::vec2 direction) {

    Character::move(direction);

    glm::vec2 tile (std::round(position[0]), std::round(position[1]));

    if (map.getItem(tile[1], tile[0])){
        return map.getItem(tile[1], tile[0])->action(true);
    }
    return nullptr ;
}
