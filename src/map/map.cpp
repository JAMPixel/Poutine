//
// Created by stiven on 17-10-21.
//

#include <map/item.h>
#include <map/map.h>
#include <fstream>
#include <inc/map/block.h>
#include <inc/utils/constant.h>
#include <iostream>
#include <inc/map/Exit.h>
#include "map/xFiles.h"

Map::Map() {
    std::srand(0);
}

void Map::createMap(std::string const &path) {
    std::ifstream file{path};
    if(!file.is_open()){
        throw std::runtime_error("file "+path+" not found");
    }
    int sizeX, sizeY;
    file >> sizeX >> sizeY;
    deltaX = constant::window::width/sizeX;
    deltaY = constant::window::height/sizeY;
    int value;
    maps.resize(sizeY);
    for(int i = 0 ; i < sizeY; i++){
        maps[i].resize(sizeX);
    }

    for (int i = 0; i < sizeY ; i++){
        for ( int j = 0; j < sizeX; j++){

            file >> value;

            if(value ){
                maps[i][j] = new Block(i, j);
            }else{
                maps[i][j] = nullptr;

            }
        }
    }
    generateItem();
}




Map::~Map() {
    for (int i = 0; i < maps.size() ; i++) {
        for (int j = 0; j < maps[0].size(); j++) {
//            delete maps[i][j];
        }
    }


}

Map::const_iterator Map::begin() const {
    return maps.begin();
}

Map::const_iterator Map::end() const {
    return maps.end();
}

int Map::getSizeX() const {
    return maps[0].size();
}

int Map::getSizeY() const {
    return maps.size();
}

int Map::getDeltaX() const {
    return deltaX;
}

int Map::getDeltaY() const {
    return deltaY;
}

 Item * const Map::getItem(int x, int y) const {
    return maps[x][y];
}

void Map::generateItem() {

    for (int i = 0; i < 2 ;) {
        int posX = std::rand() % getSizeX();
        int posY = std::rand() % getSizeY();

        if(!maps[posY][posX]){
            maps[posY][posX] = new xFiles(posX,posY);
            ++i;
            std::cout<< "  XFiles en " <<posX << " :: " << posY<< std::endl;
        }
    }
    for (int i = 0; i < 2 ;) {
        int posX = 0;
        int posY = std::rand() % getSizeY();

        if(!maps[posY][posX]){
            maps[posY][posX] = new Exit(posX,posY);
            ++i;
            std::cout<< "  Exit en " <<posX << " :: " << posY<< std::endl;
        }
    }


}
