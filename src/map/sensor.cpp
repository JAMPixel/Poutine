//
// Created by victor on 17-10-21.
//

#include <glm/geometric.hpp>
#include "inc/map/sensor.h"

sensor::sensor(float x, float y) : Item(x, y, false, "sensor") {}

int sensor::sens(glm::vec2 spyPos) {
    float dist = glm::length(pos - spyPos);
    if(dist<1.2f){
        float rx = (rand()% 4 - 2)/(100*(1+dist));
        float ry = (rand()% 4 - 2)/(100*(1+dist));

        shakePos = glm::vec2(pos.x + rx, pos.y +ry);

    }
}
