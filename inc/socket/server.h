//
// Created by stiven on 17-10-21.
//

#ifndef POUTINE_SERVER_H
#define POUTINE_SERVER_H

#include "socket.h"

class Server :public Socket{

    TCPsocket tcpsock;
    IPaddress ip;

public:

    Server();
    void connect() override ;

    virtual ~Server();

};



#endif //POUTINE_SERVER_H
