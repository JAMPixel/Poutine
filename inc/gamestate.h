//
// Created by stiven on 17-10-21.
//

#ifndef POUTINE_GAMESTATE_H
#define POUTINE_GAMESTATE_H

#include <inc/map/map.h>
#include <inc/character/character.h>
#include <inc/character/watchman.h>
#include <SDL2/SDL_quit.h>
#include <vector>
#include "character/spy.h"
#include "map/sensor.h"

class GameState{

    Map map;
    Spy spy;
    Watchman wMan;
    bool foundDocument = false;


    enum State { SPY_WIN, WATCHMAN_WIN, NOTHING};
    State state = NOTHING;

public:

    GameState();
    void loadLevel(std::string const & level);

    const Map &getMap() const;

    void update(bool isSpy, glm::vec2 direction);

    glm::vec2 manageInput(const Uint8 * key);

    void manageMouse(int x, int y, bool isSpy);

    const Spy &getSpy() const;
    const Watchman &getWatchman() const;


    std::vector<sensor> sensorList;

    void spyLoop(glm::vec2 direction);
    void guardLoop(glm::vec2 direction);

};
#endif //POUTINE_GAMESTATE_H
