//
// Created by kelmyn on 17-10-21.
//
#ifndef POUTINE_MESSENGER_H
#define POUTINE_MESSENGER_H

#include <queue>
#include "message.h"
#include <memory>
#include <inc/socket/socket.h>
#include <glm/vec2.hpp>

class Messenger
{
public:

    static void decodeTopMessage();
    static void send(Socket *socket);
    static void receive(Socket * socket);
private:
    // Ctor
    Messenger();
    // Attribs
    static Messenger instance;
    std::vector<std::unique_ptr<TypeMessage>> messageList;
    glm::vec2 * pos = nullptr;
    glm::vec2 * posXFiles= nullptr;

public:
    template<class T>
    static void send(Message<T> const & message){

        instance.messageList.emplace_back(std::make_unique<Message<T>>(message));
    }

    static std::unique_ptr<TypeMessage> getLastMessage();
    static void posMessage(glm::vec2 const & pos);
    static bool posMessage(glm::vec2  & pos);
    static void xFilesMessage(glm::vec2 const & pos);
    static bool xFilesMessage(glm::vec2  & pos);


};

#endif //POUTINE_MESSENGER_H