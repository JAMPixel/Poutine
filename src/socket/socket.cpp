//
// Created by stiven on 17-10-21.
//

#include <inc/socket/socket.h>
#include <glm/vec2.hpp>
#include <network/message.h>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <bits/unique_ptr.h>
#include <inc/network/messenger.h>

Socket::Socket(const char *ipAdress) {

    if(SDLNet_ResolveHost(&ip, ipAdress, 5000)==-1) {
        printf("SDLNet_ResolveHost: %s\n", SDLNet_GetError());
        exit(1);
    }

    tcpsock=SDLNet_TCP_Open(&ip);
    if(!tcpsock) {
        tcpsock=SDLNet_TCP_Open(&ip);
        printf("SDLNet_TCP_Open: %s\n", SDLNet_GetError());
        exit(2);
    }

}

Socket::~Socket() {
    SDLNet_TCP_Close(tcpsock);
}

Socket::Socket() {}

void Socket::connect() {

}

void Socket::send(TypeMessage *message) {
    char *toSend;
    unsigned size;
    switch (message->type){
        case NONE:
            size = sizeof(char);
            break;
        case MOVE :
            size = sizeof(float)*2;
            break;
        case XFILES:
            size = sizeof(float)*2;
            break;
        case ACT:
            break;
        case INTERACT_WITH_ENVIRONEMENT:
            break;
        case END:
            size = sizeof(char);
            break;


    }
    size*=2;
    toSend = new char[sizeof(MESSAGE_TYPE ) + size]{0};
    memcpy(toSend, (char*) &message->type, sizeof(MESSAGE_TYPE));
    char  * next = toSend+sizeof(MESSAGE_TYPE);
    glm::vec2 pos;
    float x = pos.x;
    float y = pos.y;
    char ok = 'k';
    Message<glm::vec2>* a;
    switch (message->type){
        case NONE:
            memcpy(next, &ok, sizeof(char));
            break;
        case MOVE:
            a = dynamic_cast<Message<glm::vec2>*>(message);
            pos = a->messageData;
            x = pos.x;
            y = pos.y;
            memcpy(next, &y, sizeof(float));
            memcpy(next + sizeof(float), &x, sizeof(float));
            break;
        case XFILES:
            pos = dynamic_cast<Message<glm::vec2>*>(message)->messageData;
            x = pos.x;
            y = pos.y;
            memcpy(next, &y, sizeof(float));
            memcpy(next + sizeof(float), &x, sizeof(float));
            break;
        case ACT:
            break;
        case INTERACT_WITH_ENVIRONEMENT:
            break;
        case END:
            memcpy(next, &ok, sizeof(char));
            break;

    }
    SDLNet_TCP_Send(tcpsock, toSend, size);
    delete [] toSend;

}

bool Socket::receive() {
    char rec[sizeMax]{0};
    SDLNet_TCP_Recv(tcpsock, rec, sizeMax);
    MESSAGE_TYPE type;
    memcpy(&type, rec, sizeof(MESSAGE_TYPE));
    char * next = rec+sizeof(MESSAGE_TYPE);
    glm::vec2 pos;
    float x;
    float y;
    switch (type){
        case END:
            return false;
        break;
        case MOVE:
            memcpy(&y, next, sizeof(float));
            memcpy(&x, next+sizeof(float), sizeof(float));
            Messenger::posMessage((const glm::vec2)(glm::vec2{x, y}));
            break;
        case XFILES:
            memcpy(&y, next, sizeof(float));
            memcpy(&x, next+sizeof(float), sizeof(float));
            Messenger::xFilesMessage(((const glm::vec2)(glm::vec2{x, y})));
            break;
    }

    return true;


}
